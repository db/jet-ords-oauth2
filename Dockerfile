FROM node:4-onbuild

# Create app directory
RUN mkdir -p /usr/src/app
WORKDIR /usr/src/app
 
# Install app dependencies
COPY package.json /usr/src/app
COPY Gruntfile.js /usr/src/app
 
RUN npm install
RUN npm install -g grunt-cli --allow-root
 
# Bundle app source
COPY . /usr/src/app
 
# Expose the application port
EXPOSE 8000

CMD ["grunt", "serve:release", "--force"]
